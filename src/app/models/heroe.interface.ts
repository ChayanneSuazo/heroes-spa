export interface IHeroe {
  id: string;
  name: string;
  bio: string;
  image: string;
  appearance: string;
  home: string;
}
