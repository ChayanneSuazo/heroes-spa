import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { HeroesService } from '../../services/heroes.service';
import { IHeroe } from '../../models/heroe.interface';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html',
  styleUrls: ['./search-result.component.css'],
})
export class SearchResultComponent implements OnInit {
  heroesToShow: IHeroe[] = [];
  term: string = '';

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private heroesService: HeroesService
  ) {
    this.activatedRoute.params.subscribe((params) => {
      this.term = params['term'];
      this.heroesToShow = this.heroesService.searchHeroes(params['term']);
      // console.log(params['term']);
    });
  }

  ngOnInit(): void {
    console.log(this.heroesToShow);
  }

  viewHeroe(idHeroe) {
    this.router.navigate(['/heroe', idHeroe]);
  }
}
