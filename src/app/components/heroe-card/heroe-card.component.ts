import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-heroe-card',
  templateUrl: './heroe-card.component.html',
  styleUrls: ['./heroe-card.component.css'],
})
export class HeroeCardComponent implements OnInit {
  @Input() heroe: any = {};
  @Input() idHeroe: string;

  @Output() heroeSelected: EventEmitter<string>;

  constructor(private router: Router) {
    this.heroeSelected = new EventEmitter();
  }

  ngOnInit(): void {}

  viewHero() {
    // this.heroeSelected.emit( this.idHeroe );
    this.router.navigate(['/heroe', this.idHeroe]);
  }
}
