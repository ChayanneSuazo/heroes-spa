import { Component, OnInit } from '@angular/core';
import { HeroesService } from '../../services/heroes.service';
import { IHeroe } from '../../models/heroe.interface';

// Routes
import { Router } from "@angular/router";

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.css']
})
export class HeroesComponent implements OnInit {

  heroes: IHeroe[] = [];

  constructor( private heroesService: HeroesService, private router: Router) { }

  ngOnInit(): void {
    this.heroes = this.heroesService.getHeroes();
  }

  
  viewHero(idHeroe: string) {
    this.router.navigate(['/heroe', idHeroe]);
  }
}
